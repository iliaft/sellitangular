import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {
  public URL: string = 'http://fe-kurs.light-it.net:38000/api/';
  public logInURL: string = 'http://fe-kurs.light-it.net:38000/api/login/';
  public registerURL: string = 'http://fe-kurs.light-it.net:38000/api/signup/';
  public logOutURL: string = 'http://fe-kurs.light-it.net:38000/api/logout/';
  public profileURL: string = 'http://fe-kurs.light-it.net:38000/api/profile/me/';
  public uploadPhotoURL: string = 'http://fe-kurs.light-it.net:38000/api/photo/';
  public profilePhotoURL: string = 'http://fe-kurs.light-it.net:38000/api/profile_photo/';
  public photoURL: string = 'http://fe-kurs.light-it.net:38000';
  public changePassURL: string = 'http://fe-kurs.light-it.net:38000/api/change_password/';

/*  public URL: string = 'http://fe-kurs.light-it.loc:38000/api/';
  public logInURL: string = 'http://fe-kurs.light-it.loc:38000/api/login/';
  public registerURL: string = 'http://fe-kurs.light-it.loc:38000/api/signup/';
  public logOutURL: string = 'http://fe-kurs.light-it.loc:38000/api/logout/';
  public userURL: string = 'http://fe-kurs.light-it.loc:38000/api/profile/me/';
  public uploadPhotoURL: string = 'http://fe-kurs.light-it.loc:38000/api/photo/';
  public profilePhotoURL: string = 'http://fe-kurs.light-it.loc:38000/api/profile_photo/';
  public photoURL: string = 'http://fe-kurs.light-it.loc:38000';
  public changePassURL: string = 'http://fe-kurs.light-it.loc:38000/api/change_password/';*/
}
