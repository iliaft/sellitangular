import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[scrollTop]',
})

export class ScrollTopDirective {

  constructor(private element: ElementRef) {
    this.element.nativeElement.style = 'opacity: 0';
  }

  @HostListener('click')
  public onClick() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  @HostListener('window:scroll')
  public onScroll() {
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    if (scrollTop + document.body.clientHeight >= document.body.offsetHeight + 200) {
      this.element.nativeElement.style = 'opacity: 1';
    } else {
      this.element.nativeElement.style = 'opacity: 0';
    }
  }
}
