import { Directive, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[infinityScroll]',
})

export class InfinityScrollDirective {

  @Output() public loadMore: EventEmitter<any> = new EventEmitter<any>();

  @HostListener('window:scroll', ['$event']) public onScroll() {
    let scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop || 0;

    if (scrollTop + window.innerHeight + 100 >= document.body.scrollHeight) {
      this.loadMore.emit();
    }
  }
}
