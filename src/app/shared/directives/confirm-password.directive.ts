import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';

export function confirmPassword(control: AbstractControl): { [key: string]: any }  {
  let parent = control.parent;
  if (!parent) {
    return null;
  } else if (control.parent.get('password').value !== control.value) {
    return { confirmPassword: true };
  }
}
@Directive({
  selector: '[sellitConfirmPassword]',
  providers: [{provide: NG_VALIDATORS, useExisting: ConfirmPasswordDirective, multi: true}]
})

export class ConfirmPasswordDirective implements Validator {
  public validate(control: AbstractControl) {
    return confirmPassword(control);
  }
}
