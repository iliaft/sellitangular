import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule }  from '@angular/forms';

import { EmailValidatorDirective } from './directives/email-validator.directive';
import { ConfirmPasswordDirective } from './directives/confirm-password.directive';
import { HeaderComponent } from './components/header/header.component';
import { SearchComponent } from './components/search/search.component';
import { SearchService } from './components/search/search.service';
import { UserPanelComponent } from './components/user-panel/user-panel.component';
import { FooterComponent } from './components/footer/footer.component';
import { ToTopButtonComponent } from './components/to-top-button/to-top-button.component';
import { ScrollTopDirective } from './directives/scrollTop.directive';
import { InfinityScrollDirective } from './directives/infinityScroll.directive';
import { ConfigService } from './config.service';
import { ErrorsComponent } from './components/errors/errors.component';
import { ServerErrorComponent } from './components/server-errors/server-errors.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    EmailValidatorDirective,
    ConfirmPasswordDirective,
    HeaderComponent,
    SearchComponent,
    UserPanelComponent,
    FooterComponent,
    ToTopButtonComponent,
    ErrorsComponent,
    ServerErrorComponent,
    ScrollTopDirective,
    InfinityScrollDirective
  ],
 providers: [
   ConfigService,
   SearchService
  ],
  exports: [
    ReactiveFormsModule,
    EmailValidatorDirective,
    ConfirmPasswordDirective,
    HeaderComponent,
    FooterComponent,
    ToTopButtonComponent,
    ErrorsComponent,
    ServerErrorComponent,
    ScrollTopDirective,
    InfinityScrollDirective
  ]
})

export class SharedModule {}
