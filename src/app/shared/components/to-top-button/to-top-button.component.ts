import { Component } from '@angular/core';

@Component({
  selector: 'to-top-button',
  templateUrl: './to-top-button.component.html',
  styleUrls: ['./to-top-button.component.sass']
})

export class ToTopButtonComponent {}
