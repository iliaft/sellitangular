import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'sellit-server-errors',
  templateUrl: './server-errors.component.html',
  styleUrls: ['./server-errors.component.sass']
})

export class ServerErrorComponent implements OnChanges {
  @Input() public serverError;
  public serverErrorShow= [];
  public ngOnChanges() {
    this.serverErrorShow = [];
    for (let key in this.serverError) {
      if ( this.serverError.hasOwnProperty(key) ) {
      this.serverErrorShow.push(`${key}: ${this.serverError[key][0]}`);
    } }
  }
}
