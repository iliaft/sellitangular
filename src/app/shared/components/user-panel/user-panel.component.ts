import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { MyAuthService } from '../../../core/my-auth.service';

@Component({
  selector: 'sellit-user-panel',
  styleUrls: ['./user-panel.component.sass'],
  templateUrl: './user-panel.component.html'
})
export class UserPanelComponent implements OnInit, OnDestroy {
  public profileUser: any;
  public subscription: Subscription;
  public isLogined: boolean;

  constructor(private auth: MyAuthService) {
    /**
     * Subscription on user profile data
     * @type {Subscription}
     */
    this.subscription = this.auth.listenUserProfile().subscribe((res) => {
      this.profileUser = res;
    });
  }

  /**
   * Method for loging out called by plessing button in userpanel
   */
  public logout() {
    this.auth.logOut();
  }

  /**
   * Trigger for switching user panel for logined or not logined user
   */
  public ngOnInit() {
    this.isLogined = this.auth.isLogin();
  }

  /**
   * Unsubscribing from subscription
   */
  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
