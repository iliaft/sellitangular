import { Component, Input } from '@angular/core';

@Component({
  selector: 'sellit-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.sass']
})

export class ErrorsComponent {
  @Input() public form: any;
  @Input() public myErrorType: any;

  public errors = {
    username: {
      required: 'Name is required'
    },
    title: {
      required: 'Title is required'
    },
    email: {
      required: 'E-mail is required',
      emailValidation: 'E-mail is not valid'
    },
    password: {
      required: 'Password is required',
      pattern: 'Password should consist of numbers and letters',
      minlength: 'Password should be more than 6 symbols'
    },
    password_confirm: {
      required: 'Confirm password is required',
      confirmPassword: 'Password is not equal'
    },
    description: {
      maxLength: 'Description should be shorter than 50 symbols'
    },
    price: {
      required: 'Price is required',
      pattern: 'Price should be like 10.02'
    }
  };
}
