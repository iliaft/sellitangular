import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

@Injectable()
export class SearchService {
  private subject = new Subject<any>();

  public sendSearch(searchInput: string) {
    this.subject.next({ text: searchInput });
  }

  public subscribeToSearch(): Observable<any> {
    return this.subject.debounceTime(1000);
  }
}
