import { Component } from '@angular/core';
import { SearchService } from './search.service';

@Component({
  selector: 'sellit-search',
  styleUrls: ['./search.component.sass'],
  templateUrl: './search.component.html'
})
export class SearchComponent {
  public searchInput: string = '';

  constructor(private searchService: SearchService ) {}

  public startSearch() {
    this.searchService.sendSearch(this.searchInput);
  }
}
