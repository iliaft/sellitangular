import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { SharedModule } from '../shared.module';
import { emailValidation } from '../shared/directives/email-validator.directive';
import { confirmPassword } from '../shared/directives/confirm-password.directive';
import { MyAuthService } from '../core/my-auth.service';
import { RegistrationModel } from '../core/registration.model';
import { LoginModel } from '../core/login.model';

@Component({
  selector: 'sellit-login-page',
  styleUrls: ['./login-page.component.sass'],
  templateUrl: './login-page.component.html'
})
export class LoginComponent {
  public ifRegister: boolean = false;
  public serverError: any;

  public reactiveLoginForm: FormGroup = new FormGroup({
    email: new FormControl(null, [Validators.required, emailValidation]),
    password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
  });

  public reactiveRegistrationForm: FormGroup = new FormGroup({
    email: new FormControl(null, [Validators.required, emailValidation]),
    username: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
    password_confirm: new FormControl(null, [Validators.required, confirmPassword])
  });

  constructor( private auth: MyAuthService, private router: Router ) {};

  /**
   * Method for switching between login and register forms
   */
  public showRegister() {
    this.ifRegister = true;
  }
  /**
   * Method for switching between login and register forms
   */
  public showLogin() {
    this.ifRegister = false;
  }

  /**
   * Metod called after pressing submit button, calls login or register methods
   * parametrs are $event and form data
   */
  public submit($event, form) {
    $event.preventDefault();
    let target = $event.target || $event.srcElement;
    if ( target.id === 'loginForm' ) {
      this.auth.logIn(new LoginModel(form.value)).subscribe((res) => {
        this.router.navigate(['/home-page']);
      }, (error) => {
        this.serverError = error.json();
      });
    }
    if ( target.id === 'registrationForm' ) {
      this.auth.register(new RegistrationModel(form.value)).subscribe((res) => {
      }, (error) => {
        this.serverError = error.json();
      });
    }
  }
}
