import { Http, Request, RequestOptionsArgs, Response, RequestOptions, ConnectionBackend } from '@angular/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { SessionService } from './session.service';

@Injectable()
export class MyHttp extends Http {

  constructor(_backend: ConnectionBackend,
              _defaultOptions: RequestOptions,
              private mySession: SessionService) {
    super(_backend, _defaultOptions );
  }

  /**
   * Adding authorization token of logined user to header of any http-request
   */
  public request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
    if (this.mySession.token) {
      if (options) {
        options.headers.set('Authorization', 'Token ' + this.mySession.token);
      } else {
        (<Request> url).headers.set('Authorization', 'Token ' + this.mySession.token);
      }
    }
    return super.request(url, options);
  }
}
