import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { SessionService } from './session.service';
import { ConfigService } from '../shared/config.service';

@Injectable()
export class MyAuthService {
  public userProfile = new BehaviorSubject(null);

  constructor(private http: Http,
              private config: ConfigService,
              private session: SessionService) {
    this.userProfile.next(this.session.userLocalStorage);
  }

  /**
   * Send http-request for registration
   * @param data from registration form
   * @returns success/error message
   */
  public register(data) {
    return this.http.post( this.config.registerURL, data)
      .map((res: Response) => {
      return res;
    });
  }

  /**
   * Send http-request for loggin in
   * @param data from logIn form
   * @returns object with information about user
   */
  public logIn(data) {
    return this.http.post(this.config.logInURL, data)
      .map((res: Response) => {
        this.session.token = res.json().token;
        this.getUserData().subscribe((resul) => {
          this.session.userLocalStorage = resul;
          this.userProfile.next(this.session.userLocalStorage);
        });
        return res;
      });
  }

  /**
   * Send http-request for all data about user
   * @returns object with all information about user
   */
  public getUserData() {
    return this.http.get(this.config.profileURL)
      .map((res) => {
        return res.json();
      });
  }

  /**
   * Method for updating data for userProfile
   */
  public updateProfile() {
    this.getUserData().subscribe((res) => {
      this.session.userLocalStorage = res;
      this.userProfile.next(this.session.userLocalStorage);
    });
  };

  /**
   * Method for subscribing on user profile information in case changing it
   * @returns {BehaviorSubject} with user data
   */
  public listenUserProfile(): Observable<any> {
    return this.userProfile;
  }

  /**
   * Checking is user logined
   * @returns true if logined( token exist)
   */
  public isLogin() {
    return !!this.session.token;
  }

  /**
   * Cleans cookie, local storage and send http-request for log out
   */
  public logOut() {
    this.session.delCookie();
    this.session.userLocalStorage = null;
    this.http.post( this.config.logOutURL, '');
  }
}
