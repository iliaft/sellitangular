import { Injectable } from '@angular/core';
import { Cookie } from 'ng2-cookies';

import { User } from './user.model';
import { ConfigService } from '../shared/config.service';

@Injectable()
export class SessionService {
  public cookies: Object;
  public keys = [];
  public cookieName: string = 'sellitToken';

  constructor(private config: ConfigService) {
    this.update();
  }

  /**
   * function for updating the cookies
   */
  public update() {
    this.cookies = Cookie.getAll();
    this.keys = Object.keys(this.cookies);
  }

  /**
   * function for deleting the cookies
   */
  public delCookie() {
    Cookie.delete(this.cookieName);
    this.update();
  };

  /**
   * setter that sets token to the cookie with name cookieName
   */
  set token(data) {
    Cookie.set(this.cookieName, data);
    this.update();
  }

  /**
   * getter that gets token from cookie with name cookieName
   */
  get token() {
    return Cookie.get(this.cookieName);
  }

  /**
   * getter that retrievs data about user from local storage
   */
  get userLocalStorage() {
    return JSON.parse(localStorage.getItem('user'));
  }

  /**
   * setter that saves data about user to local storage,
   * in case data==null it erase user data( while calling logout method in my-auth servise)
   * @param data is data from server or null while logOut
   */
  set userLocalStorage(data) {
    if (data) {
      let item = this.updatePhoto(data);
      localStorage.setItem('user', JSON.stringify(item));
    } else {
      localStorage.setItem('user', JSON.stringify(data));
    }
  }

  /**
   * trick for recording url of avatar photos to user profile in correct format
   */
  public updatePhoto(data) {
    let item = new User(data);
    item.photo = `${this.config.photoURL}${item.photo.photo}`;
    return item;
  }
}
