import { Injector, Injectable, ReflectiveInjector } from '@angular/core';
import { async, getTestBed, TestBed } from '@angular/core/testing';
import { BaseRequestOptions, Http, Response, ResponseOptions, XHRBackend } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { ConfigService } from '../shared/config.service';
import { User } from '../../shared/models/user.model';
import { Cookie } from 'ng2-cookies';
import { SessionService } from './session.service';
import { MyAuthService } from './my-auth.service';

describe('Service: Auth', () => {

  let backend: MockBackend;
  let service: MyAuthService;
  let sessionService: SessionService;
  let testbed: Injector;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
      ],
      providers: [
        BaseRequestOptions,
        MockBackend,
        ConfigService,
        SessionService,
        MyAuthService,
        {
          deps: [
            MockBackend,
            BaseRequestOptions
          ],
          provide: Http,
          useFactory: (backend2: XHRBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backend2, defaultOptions);
          }
        }
      ]
    });

    testbed = getTestBed();
    backend = testbed.get(MockBackend);
    service = testbed.get(MyAuthService);
    sessionService = testbed.get(SessionService);

  }));

  afterEach(() => {

    testbed = undefined;
    sessionService = undefined;

  });

  function logInBackend(backend1: MockBackend, options: any) {
    backend1.connections.subscribe((connection: MockConnection) => {
      if (connection.request.url === 'http://fe-kurs.light-it.net:38000/api/login/') {
        const responseOptions = new ResponseOptions(options);
        const response = new Response(responseOptions);
        connection.mockRespond(response);
      }
    });
  }

  describe('Feature: isLog', () => {

    let isLog: boolean;

    describe('WHEN, token not exist', () => {
      beforeEach(() => {
        spyOnProperty(sessionService, 'token', 'get').and.returnValue(false);
        isLog = service.isLogin();
      });
      it('THEN, method return false', () => {
        expect(isLog).toBeFalsy();
      });
    });

    describe('WHEN, token exist', () => {
      beforeEach(() => {
        spyOnProperty(sessionService, 'token', 'get').and.returnValue('token');
        isLog = service.isLogin();
      });
      it('THEN, method return true', () => {
        expect(isLog).toBeTruthy();
      });
    });
  });

  describe('Feature: logIn', () => {
    describe('WHEN, user is exist', () => {
      let callback: jasmine.Spy;

      beforeEach(() => {
        callback = jasmine.createSpy('callback');

        logInBackend(backend, {
          body: [
            {
              id: 123,
              first_name: '',
              last_name: '',
              email: 'test@email.co',
              username: 'test',
              token: 'some-token-value'
            }
          ],
          status: 200
        });

        let data = {
          email: '123@123.io',
          password: '123123'
        };

        service.logIn(data).subscribe((resp) => {
          callback(resp);
        });
      });

      it('THEN, method return error', () => {
        expect(callback).toHaveBeenCalled();
      });

    });

/*    describe('WHEN, user is not exist', () => {
      beforeEach(() => {

      });
      it('THEN, method return user', () => {

      });
    });*/
  });
/*  describe('Feature: LogOut', () => {
    describe('Loged out', () => {
      beforeEach(() => {
        spyOnProperty(sessionService, 'token', 'get').and.returnValue('some-token-value');
        spyOnProperty(sessionService, 'userLocalStorage', 'get').and.returnValue({id: 123, username: 'test'});
      });
      it('THEN, method clear session and send event', (resp) => {
        service.logOut();
        expect(sessionService.token).toBeNull();
        expect(sessionService.userLocalStorage).toBeNull();
      });
    });
  });*/
});
