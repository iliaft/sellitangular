export class RegistrationModel {
  public email: string;
  public username: string;
  public password: string;
  public passwordConfirm: string;
  constructor(data) {
    this.email = data.email;
    this.username = data.username;
    this.password = data.password;
    this.passwordConfirm = data.password_confirm;
  }
}
