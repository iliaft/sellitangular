import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs';

import { ConfigService } from '../shared/config.service';
import { SessionService } from './session.service';

@Injectable()
export class ProfileService {

  constructor(private http: Http,
              private config: ConfigService,
              private session: SessionService) {}

  /**
   * Method for uploading photo to the server
   * @param data is avatar-photo from user-profile.component form
   * @returns method with http-request for adding photo to user profile on server
   */
  public uploadPhoto(data) {
    return this.http.post(this.config.uploadPhotoURL, data)
      .toPromise()
      .then ((res) => { return this.updateProfilePhoto(res.json()); }
      );
  }

  /**
   * Method with http-request for adding downloaded photo to user profile on server
   * @param res is downloaded in user-profile photo
   * @returns method with http-request for getting updated user information
   */
  public updateProfilePhoto(res) {
    let result = res[0].id;
    let obj = {
      user: this.session.userLocalStorage.id,
      photo: result
    };
    return this.http.post(this.config.profilePhotoURL, obj)
      .toPromise();
  }

  /**
   * Sending http-request for changin password
   * @param obj with old password? confirmation and new assword
   */
  public changePassword(obj) {
    return this.http.post(this.config.changePassURL, obj)
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: any) => { return Observable.throw(error.json()); });
  }

}
