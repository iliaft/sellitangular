import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MyAuthService } from './my-auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: MyAuthService,
              /*private router: Router*/  ) {}

  public canActivate( /*route: ActivatedRouteSnapshot, state: RouterStateSnapshot*/ ) {
    if (this.authService.isLogin()) { return true; }
  }

}
