import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { ProductsService } from './product.service';
import { SharedModule } from '../shared.module';
import { SearchService } from '../shared/components/search/search.service';

@Component({
  selector: 'sellit-home-page',

  templateUrl: './home-page.component.html'
})
export class HomeComponent implements OnInit, OnDestroy {
  public subscription: Subscription;
  public searchInput: any;
  public products = [];
  private offset: number|string = '0';

  constructor( private product: ProductsService,
               private search: SearchService ) {
    this.subscription = this.search.subscribeToSearch()
      .subscribe((searchInput) => {
        this.searchInput = searchInput.text;
        this.product.getProducts('0', this.searchInput).subscribe((res) => {
          this.products = [];
          res.forEach((item) => {
            this.products.push(item);
          });
        });
      });
  }

  /**
   * Method called by infinity scroll directive for
   * calling method from product service for requesting data with products from server
   */
  public addItems() {
    this.offset = +this.offset + 12;
    this.product.getProducts(this.offset).subscribe((res) => {
      res.forEach((item) => {
        this.products.push(item);
      });
    });
  }

  public ngOnInit() {
    this.product.getProducts(0).subscribe((res) => {
      this.products = res;
    });
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
