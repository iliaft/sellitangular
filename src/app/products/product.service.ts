import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Product } from './models/product.model';
import { ConfigService } from '../shared/config.service';
import { SessionService } from '../core/session.service';

@Injectable()
export class ProductsService {
  public filesPhoto: any;
  private limit: string = '12';
  private offset: number|string = '0';

  constructor(private http: Http,
              private config: ConfigService,
              private session: SessionService) {}

  /**
   * Sends http-request for getting data for list of products,
   * or, in case of unnecessary parametr searchInput, products were searching
   */
  public getProducts(offset, ...searchInput): Observable<Product[]> {
    let params: URLSearchParams = new URLSearchParams();
    if (!!searchInput) {
      params.set('limit', '100');
      params.set('offset', '0');
      params.set('search', searchInput.toString());
      return this.http.get(`${this.config.URL}poster/`, {search: params})
        .map((res: Response) => {
          let tmp = [];
          let data = res.json().results;
          data.forEach((item) => {
            tmp.push(new Product(item));
          });
          return tmp;
        });
    } else {
      params.set('limit', this.limit);
      params.set('offset', offset.toString());
      return this.http.get(`${this.config.URL}poster/`, {search: params})
        .map((res: Response) => {
          let tmp = [];
          let data = res.json().results;
          data.forEach((item) => {
            tmp.push(new Product(item));
          });
          return tmp;
        });
    }
  }

  /**
   * Sends http-request for getting all data about single product
   */
  public getSingleProduct(id): Observable<Product> {
    return this.http.get(`${this.config.URL}poster/${id}/`)
      .map((res: Response) => {
        let data = res.json();
        return new Product(data);
      });
  }

  /**
   * Method for product editing
   * @param id of product
   * @param editForm - object with data from edit product component
   * @param photo - photos from edit product component
   */
  public editProduct(id, editForm, photo) {
    console.log('photo -', photo);
    let editProductObj = {
      title: editForm.value.title,
      description: editForm.value.description,
      author: this.session.userLocalStorage.id,
      price: editForm.value.price,
      photos: []
    };
    return this.newPhoto(photo).flatMap(
      (res) => {
        let photosId = [];
        for (let item of res.json()) {
          photosId.push(item.id);
        }
        editProductObj.photos = photosId;
        return this.http.put(`${this.config.URL}poster/${id}/`, editProductObj)
          .map((resss: Response) => {
            return resss.json();
          });
      });
  }

  /**
   * Method for Deleting product http-request
   * @param id is id of deleted product
   */
  public deleteProduct(id) {
    return this.http.delete(`${this.config.URL}poster/${id}`) // add slash for correct working ..er/${id}/`)
      .map((res: Response) => {
        return res.json();
      });
  }

  /**
   * Method for creating a new product, returns Object with Product
   */
  public addProduct(form, photos) {
    let addProductObj = {
      title: form.value.title,
      description: form.value.description,
      price: form.value.price,
      author: this.session.userLocalStorage.id,
      date_create: new Date(),
      photos: []
    };
    return this.newPhoto(photos).flatMap(
      (res) => {
        let photosId = [];
        for (let item of res.json()) {
          photosId.push(item.id);
        }
        addProductObj.photos = photosId;
        return this.newProduct(addProductObj);
      });
  }

  /**
   * Sends http-request for creating a new post
   * @param data is required Object with data
   */
  private newProduct(data) {
    return this.http.post(`${this.config.URL}poster/`, data)
      .map((result: Response) => {
        return result.json();
      });
  }

  /**
   * Sends http-request for uploading photos
   * @param photos
   */
  private newPhoto(photos) {
    if (photos) {
      let files: FileList = photos;
      this.filesPhoto = new FormData();
      for (let i in files) {
        if (files.hasOwnProperty(i)) {
          this.filesPhoto.append('photo', files[i]);
        }
      }
    }
    return this.http.post(`${this.config.URL}photo/`, this.filesPhoto)
      .map((res: Response) => {
        return res;
      });
  }
}
