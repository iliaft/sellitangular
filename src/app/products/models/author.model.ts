export class Author {
  public id: number;
  public firstName: string;
  public lastName: string;
  public email: string;
  public userName: string;
  public photo: any;

  constructor(data) {
    this.id = data.id;
    this.firstName = data.first_name;
    this.lastName = data.last_name;
    this.email = data.email;
    this.userName = data.username;
    this.photo = data.photo;
  }

}
