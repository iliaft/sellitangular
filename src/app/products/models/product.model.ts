import { Author } from './author.model';

export class Product {
  public id: number;
  public title: string;
  public description: string;
  public author: Author;
  public price: string;
  public photoDetails: any;
  public dateCreate: string;
  public dateUpdate: string;
  constructor(data) {
    this.id = data.id;
    this.title = data.title;
    this.description = data.description;
    this.author = new Author(data.author_details);
    this.price = data.price;
    this.photoDetails = data.photo_details;
    this.dateCreate = data.date_create;
    this.dateUpdate = data.date_update;
  }
}
