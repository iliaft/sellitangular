import { Routes } from '@angular/router';

import { DetailComponent } from './product-detail/detail-page.component';
import { ProductEditComponent } from './product-edit/product-edit.component';

export const productRoutes: Routes = [
//  { path: 'detail-page', component: DetailComponent },
  { path: 'detail-page/:id', component: DetailComponent },
  { path: 'product-edit/:id', component: ProductEditComponent }
];
