import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ProductsService } from '../product.service';

@Component({
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.sass']
})

export class ProductEditComponent implements OnInit {
  public id: number;
  public product: any;
  public photos: any;
  public priceExp = /^\d{1,8}(\.\d{2})?$/;
  public editorSettings = { heightMin: 100, heightMax: 200,  };

  public reactiveProductEditForm: FormGroup = new FormGroup({
    title: new FormControl(null, [Validators.required]),
    price: new FormControl(null, [Validators.required, Validators.pattern(this.priceExp)]),
    description: new FormControl(null)
  });

  constructor ( public productService: ProductsService, private router: Router, public route: ActivatedRoute ) {
    route.params.subscribe((res) => {
      this.id = res.id;
    });
  }

  /**
   * Edit product after pressing button
   * @param $event and form - inputed data
   */
  public submit($event, form ) {
    $event.preventDefault();
    this.productService.editProduct( this.id, form, this.photos ).subscribe((res) => {
      console.log ('response - ', res);
      this.router.navigate(['detail-page/', res.id]);
    });
  }

  /**
   * Method that assigns photo from edit-product form to variable
   */
  public getFiles($event) {
    let target = $event.target || $event.srcElement;
    this.photos = target.files;
  }

  public ngOnInit() {
    this.productService.getSingleProduct(this.id).subscribe((resp) => {
      this.product = resp;
      this.reactiveProductEditForm.patchValue({
        title: this.product.title,
        price: this.product.price,
        description: this.product.description
      });
      console.log('product = ', this.product);
    });
  }
}
