import { Component, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ProductsService } from '../product.service';

@Component({
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.sass']
})

export class ProductAddComponent {
  public photos: any;
  public photoArray = [];
  public priceExp = /^\d{1,8}(\.\d{2})?$/;
  public editorSettings = {heightMin: 100, heightMax: 200, };

  public reactiveProductForm: FormGroup = new FormGroup({
    title: new FormControl(null, [Validators.required]),
    price: new FormControl(null, [Validators.required, Validators.pattern(this.priceExp)]),
    description: new FormControl(null)
  });

  constructor(public product: ProductsService,
              private router: Router,
              private element: ElementRef) { }

  /**
   * Add new product after pressing button
   * @param $event and form - inputed data
   */
  public submit($event, form) {
    $event.preventDefault();
    this.product.addProduct(form, this.photos).subscribe((res) => {
      console.log('responce - ', res);
      this.router.navigate(['detail-page/', res.id]);
    });
  }

  /**
   * Method that assigns photo from add-product form to variable
   */
  public getFiles($event) {
    this.photoArray = [];
    let target = $event.target || $event.srcElement;
    this.photos = target.files;
    for (let i = 0; i < this.photos.length; i++) {
    let reader = new FileReader();
    reader.onload = (e: any) => {
      this.photoArray[i] = e.target.result;
    };
    reader.readAsDataURL(target.files[i]);
    this.photoArray.push(this.photos[i]);
    }

  }
}
