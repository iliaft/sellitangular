import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule }  from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';
import { ChatModule } from '../chat/chat.module';

import { ProductAddComponent } from './product-add/product-add.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { HomeComponent } from './home-page.component';
import { DetailComponent } from './product-detail/detail-page.component';
import { ListItemComponent } from './list-item/list-item.component';
import { CarouselComponent } from './product-detail/carousel/carousel.component';

import { ProductsService } from './product.service';
import { productRoutes } from './product.routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(productRoutes),
    NgbModule,
    BrowserAnimationsModule,
    FroalaEditorModule,
    FroalaViewModule,
    SharedModule,
    ChatModule,
    ReactiveFormsModule
  ],
  declarations: [
    ProductAddComponent,
    ProductEditComponent,
    HomeComponent,
    CarouselComponent,
    DetailComponent,
    ListItemComponent
  ],
  providers: [
    ProductsService,
  ],
  exports: [
    HomeComponent,
    CarouselComponent,
    DetailComponent,
    ListItemComponent,
    ProductAddComponent,
    ProductEditComponent,
    RouterModule
  ]
})

export class ProductModule {}
