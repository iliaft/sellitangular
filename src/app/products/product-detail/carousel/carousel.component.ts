import { Component, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'sellit-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.sass'],
  providers: [NgbCarouselConfig]
})
export class CarouselComponent {
  @Input() public product;
  constructor(private config: NgbCarouselConfig) {
    config.interval = 5000;
  }
}
