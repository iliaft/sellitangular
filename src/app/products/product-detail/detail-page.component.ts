import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/map';
import swal from 'sweetalert2';

import { ProductsService } from '../product.service';
import { SharedModule } from '../shared.module';
import { MyAuthService } from '../../core/my-auth.service';
import { SessionService } from '../../core/session.service';

@Component({
  selector: 'sellit-detail-page',

  templateUrl: './detail-page.component.html'
})
export class DetailComponent implements OnInit {
  public product = {};
  public id: number;
  public isLogged: boolean;
  public isOwner: boolean = false;

  constructor(public route: ActivatedRoute,
              private routeDirect: Router,
              private productService: ProductsService,
              private session: SessionService,
              private auth: MyAuthService) {
    route.params.subscribe((res) => {
      this.id = res.id;
    });
  }

  /**
   * Method for deleting product with sweet alert
   */
  public deleteProduct() {
    this.productService.deleteProduct(this.id).subscribe(
      (res) => {
        swal({
          title: 'Success',
          text: 'Product was successfully deleted!',
          type: 'success',
          confirmButtonText: 'OK!',
          buttonsStyling: false
        }).then(() => {
          this.routeDirect.navigate(['/home-page']);
        }, (dismiss) => {
          this.routeDirect.navigate(['/home-page']);
        });
      },
      (error) => {
        swal({
          title: 'Error',
          text: 'Product was not deleted because absence / in product.servise 79',
          type: 'error',
          confirmButtonText: 'Try after fix',
          buttonsStyling: false
        }).then(() => {
          let navigate = '/detail-page/' + this.id;
          this.routeDirect.navigate([navigate]);
        }, (dismiss) => {
          let navigate = '/detail-page/' + this.id;
          this.routeDirect.navigate([navigate]);
        });
      });
  }

  public ngOnInit() {
    this.isLogged = this.auth.isLogin();
    this.productService.getSingleProduct(this.id).subscribe((res) => {
      this.product = res;
      console.log('product = ', this.product);
      if (this.isLogged) {
        if (res.author.id === this.session.userLocalStorage.id) {
          this.isOwner = true;
        }
      }
    });
  }
}
