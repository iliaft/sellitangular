import { Component, Input } from '@angular/core';

@Component({
  selector: 'sellit-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.sass']
})

export class ListItemComponent {
  @Input() public products = [];
}
