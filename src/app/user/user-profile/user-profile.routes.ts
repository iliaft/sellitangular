import { Routes } from '@angular/router';

import { UserProfileCanDeactivate } from './user-profile.candeactivate-guard';
import { UserProfileComponent } from './user-profile.component';

export const userRoutes: Routes = [
  {path: 'user-profile', component: UserProfileComponent,
    pathMatch: 'full', canDeactivate: [UserProfileCanDeactivate]}
];
