import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import { confirmPassword } from '../../../shared/directives/confirm-password.directive';
import { MyAuthService } from '../../../core/my-auth.service';
import { ProfileService } from '../../../core/profile.service';
import { SharedModule } from '../shared.module';

@Component({
  selector: 'sellit-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.sass']
})
export class ChangePasswordComponent {
  public serverError: any;
  public passExp = /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[a-z]).*$/;

  public reactiveProfileForm: FormGroup = new FormGroup({
    password: new FormControl(
      null, [Validators.required,
        Validators.pattern(this.passExp),
        Validators.minLength(6)]),
    password_confirm: new FormControl(null, [Validators.required, confirmPassword]),
    password_old: new FormControl(null, [Validators.required])
  });

  constructor( private profileService: ProfileService,
               private auth: MyAuthService,
               public activeModal: NgbActiveModal,
               private router: Router ) {}

  /**
   * Method sends old and new passwords to server
   */
  public submit($event, form) {
    if (form.invalid) {
      alert('Wrong input data');
    }
    let obj = {
      new_password1: form.value.password,
      new_password2: form.value.password_confirm,
      old_password: form.value.password_old
    };
    return this.profileService.changePassword(obj).subscribe((res) => {
      this.activeModal.close();
      this.auth.logOut();
      this.router.navigate(['/login-page']);
    }, (error) => {
      this.serverError = error;
    });
  }

}
