import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ProfileService } from '../../core/profile.service';
import { MyAuthService } from '../../core/my-auth.service';
import { ChangePasswordComponent } from './change-password/change-password.component';

@Component({
  selector: 'sellit-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.sass']
})
export class UserProfileComponent implements OnInit, OnDestroy {

  public profilePhoto: any;
  public fileForm: any;
  public subscribeOnUser: Subscription;
  public userData: any;

  constructor(private auth: MyAuthService,
              private profileService: ProfileService,
              private modal: NgbModal,
              private element: ElementRef) {}

  /**
   * Call modal window
   */
  public openChangePass(): void {
    const modal = this.modal.open(ChangePasswordComponent);
  }

  /**
   * Method witch called by save button for changing profile photo
   */
  public changePhoto() {
    if (this.profilePhoto) {
      let files: FileList = this.profilePhoto;
      this.fileForm = new FormData();
      this.fileForm.append('photo', files[0]);
      this.profileService.uploadPhoto(this.fileForm)
        .then((res) => {
          console.log('upload response - ', res);
          this.auth.updateProfile();
        });
    }
  }

  /**
   * Method witch gives photo file to variable profilePhoto
   * @param $event
   */
  public getFiles($event) {
    let target = $event.target || $event.srcElement;
    this.profilePhoto = target.files;
    let reader = new FileReader();
    let image = this.element.nativeElement.querySelector('.new-image');
    reader.onload = (e: any) => {
      image.src = e.target.result;
    };
    reader.readAsDataURL(target.files[0]);
  }

  public ngOnInit() {
    this.subscribeOnUser = this.auth.listenUserProfile()
      .subscribe((res) => {
        this.userData = res;
      });
  }

  public ngOnDestroy() {
    this.subscribeOnUser.unsubscribe();
  }

}
