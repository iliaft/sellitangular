import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserProfileComponent } from './user-profile.component';

@Injectable()
export class UserProfileCanDeactivate implements CanDeactivate<UserProfileComponent> {
  public canDeactivate(component: UserProfileComponent,
                       currentRoute: ActivatedRouteSnapshot,
                       currentState: RouterStateSnapshot,
                       nextState?: RouterStateSnapshot): boolean |
                       Observable<boolean> | Promise<boolean>  {
    return confirm('Несохранённые данные будут утрачены');
  }
}
