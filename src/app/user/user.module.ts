import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule }  from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ChangePasswordComponent } from './user-profile/change-password/change-password.component';
import { userRoutes } from './user-profile/user-profile.routes';
import { UserProfileCanDeactivate } from './user-profile/user-profile.candeactivate-guard';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(userRoutes),
    SharedModule,
    ReactiveFormsModule,
    NgbModule
  ],
  declarations: [
    UserProfileComponent,
    ChangePasswordComponent
  ],
  providers: [UserProfileCanDeactivate],
  entryComponents: [ChangePasswordComponent],
  exports: [ RouterModule ]
})

export class UserProfileModule {}
