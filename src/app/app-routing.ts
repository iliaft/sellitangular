import { Routes } from '@angular/router';
import { HomeComponent } from './products/home-page.component';
import { LoginComponent } from './login-page/login-page.component';
import { PageNotFoundComponent } from './404/not-found.component';
import { ProductAddComponent } from './products/product-add/product-add.component';
import { AuthGuard } from './core/authGuard';

export const appRoutes: Routes = [
  { path: 'home-page', component: HomeComponent },
  { path: 'login-page', component: LoginComponent },
  { path: 'product-add', component: ProductAddComponent, canActivate: [AuthGuard] },
  { path: 'user-profile', loadChildren: 'app/user/user.module#UserProfileModule', canActivate: [AuthGuard] },
  { path: '',   redirectTo: '/home-page', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];
