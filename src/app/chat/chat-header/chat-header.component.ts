import { Component } from '@angular/core';

@Component({
  selector: 'sellit-chat-header',
  templateUrl: './chat-header.component.html',
  styleUrls: ['./chat-header.component.sass']
})

export class ChatHeaderComponent {}
