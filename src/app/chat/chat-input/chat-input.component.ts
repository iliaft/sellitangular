import { Component } from '@angular/core';

@Component({
  selector: 'sellit-chat-input',
  templateUrl: './chat-input.component.html',
  styleUrls: ['./chat-input.component.sass']
})

export class ChatInputComponent {}
