import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatHeaderComponent } from './chat-header/chat-header.component';
import { ChatUsersComponent } from './chat-users/chat-users.component';
import { ChatHistoryComponent } from './chat-history/chat-history.component';
import { ChatInputComponent } from './chat-input/chat-input.component';
import { ChatComponent } from './chat.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ChatHeaderComponent,
    ChatUsersComponent,
    ChatHistoryComponent,
    ChatInputComponent,
    ChatComponent
  ],
  providers: [],
  exports: [
    ChatComponent
  ]
})

export class ChatModule {
}
